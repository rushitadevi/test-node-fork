import { connect } from 'mongoose'
//to handle HTTP request in express we need to install middleware which is body parser 
import bodyParser from 'body-parser' 
// express is framework which helps to route everything
import express from 'express'
//user Model is schema which we need to import
import UserModel from './UserModel'
//cors allowes some restricted resources to be requested from other server
import cors from 'cors'

//connecting with mongo database by this url 
connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

//to fetch all users from database
app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})

//this is to create new record in the database.
app.post('/users', (req, res) => {
  //initialise new user schema model
  let user = new UserModel()

  //taking all the values from the input
  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  //saving that data in the database. if some error occurs from the database, it will be fetched in err
  //or else new user will be saved in database.
  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

//Added by Rushita
//deleting user by id
app.delete('/users/:id', function (req, res) {
  //checking and deleteing that user by using findIdAndDelete method
  //if in the DB, id is not present then show message that id not found.
  UserModel.findByIdAndDelete(req.params.id, function (err, response) {
    if (err) {
      res.send(err)
    } else {
      if (response === null) {
        //if response is null then return user not found.
        res.send(`User with Id ${req.params.id} not found`)
      } else {
        res.send('User deleted successfully.')
      }
    }
  })
})

//to update user=> taking user inut and updating it by checking id
app.put('/users/:id', function (req, res) {
  //checking given id in the DB and updating that user by using findByIdAndUpdate method
  //if in the DB, id is not present then show message that id not found.
  UserModel.findByIdAndUpdate({ _id: req.params.id }, { $set: req.body }, function (err, response) {
    if (err) {
      res.send(err)
    } else {
      if (response === null) {
        //if response is null then return user not found.
        res.send(`User with Id ${req.params.id} not found`)
      } else {
        res.send('User updated successfully.')
      }
    }
  })
})

//listen to port 8080
app.listen(8080, () => console.log('Example app listening on port 8080!'))
